##
# Spike
#
# @file
# @version 0.1

all: bin/frag.spv bin/vert.spv

bin/frag.spv: src/shaders/shader.frag
	glslc src/shaders/shader.frag -o bin/frag.spv
bin/vert.spv: src/shaders/shader.vert
	glslc src/shaders/shader.vert -o bin/vert.spv

clean:
	rm -f bin/*
# end
