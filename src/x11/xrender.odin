package x11

//NOTE This part of the package may break in the (unlikely) event that XRender changes
import "vendor:x11/xlib"

foreign import Xrender "system:Xrender"
foreign import Xfixes "system:Xfixes"
foreign Xrender {
		XRenderFindVisualFormat :: proc "c" (dpy: ^xlib.Display, visual: ^xlib.Visual) -> ^XRenderPictFormat ---
}
foreign Xfixes {
	XFixesHideCursor :: proc "c" (dpy: ^xlib.Display, win: xlib.Window) ---
}

PictFormat :: xlib.XID
PictTypeDirect : i32 : 1
TrueColor : i32 : 4

XRenderPictFormat :: struct {
		id: PictFormat,
		type: i32,
		depth: i32,
		direct: XRenderDirectFormat,
		colormap: xlib.Colormap,
}
XRenderDirectFormat :: struct {
		red: u16,
		redMask: u16,
		green: u16,
		greenMask: u16,
		blue: u16,
		blueMask: u16,
		alpha: u16,
		alphaMask: u16,
}

XRenderGetVisual :: proc (dpy: ^xlib.Display, depth: i32) -> (vis: ^xlib.Visual) {
		visinfo := xlib.XVisualInfo{
				screen = 0,
				depth = depth,
				class = TrueColor
		}

		vis = nil
		visualCount : i32
		xvi := xlib.XGetVisualInfo(dpy, {.VisualScreenMask, .VisualDepthMask, .VisualClassMask}, &visinfo, &visualCount)

		if visualCount == 0 do return vis


		for i in 0..<visualCount {
				fmt := XRenderFindVisualFormat(dpy, xvi[i].visual)

				if fmt.type == PictTypeDirect && fmt.direct.alphaMask != 0 {
						vis = xvi[i].visual
						break
				}
		}
		xlib.XFree(xvi)
		return vis
}
