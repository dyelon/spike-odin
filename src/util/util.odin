package util

import "core:fmt"
import "core:math"
import "core:math/fixed"
import "core:os"

BMPCompression :: enum u32 {
	BI_RGB,
	BI_RLE8,
	BI_RLE4,
	BI_BITFIELDS,
	BI_JPEG,
	BI_PNG,
	BI_ALPHABITFIELDS,
	BI_CMYK,
	BI_CMYKRLE8,
	BI_CMYKRLE4,
}

CIEXYZ :: struct {
	x: fixed.Fixed(u32, 30),
	y: fixed.Fixed(u32, 30),
	z: fixed.Fixed(u32, 30),
}

CIEXYZTriple :: struct {
	red: CIEXYZ,
	green: CIEXYZ,
	blue: CIEXYZ,
}

ColorSpace :: enum u32 {
	LCS_CALIBRATED,
	LCS_sRGB = 0x73524742,
	LCS_WINDOWS_COLOR_SPACE = 0x57696E20,
	LINKED = 'L' << 24 | 'I' << 16 | 'N' << 8 | 'K',
	EMBEDDED = 'M' << 24 | 'B' << 16 | 'E' << 8 | 'D',
}

Intent :: enum u32 {
	LCS_GM_BUSINESS = 1,
	LCS_GM_GRAPHICS = 2,
	LCS_GM_IMAGES = 4,
	LCS_GM_ABS_COLORMETRIC = 8,
}

V5Head :: struct #packed {
	head: [2]u8,
	size: u32,
	res1: [2]u8,
	res2: [2]u8,
	pixelOffset: u32,
	headerSize: u32,
	width: i32,
	height: i32,
	planes: u16,
	bitsPerPixel: u16,
	compression: BMPCompression,
	imageSize: u32,
	horizontalRes: i32,
	verticalRes: i32,
	palette: u32,
	importantColors: u32,
	redMask: u32,
	greenMask: u32,
	blueMask: u32,
	alphaMask: u32,
	colorSpace: ColorSpace,
	endpoints: CIEXYZTriple,
	gammaRed: u32,
	gammaGreen: u32,
	gammaBlue: u32,
	intent: Intent,
	profileDataOffset: u32,
	profileSize: u32,
	res3: u32,
}

BMPHeaderVersion :: union #shared_nil {
		^V5Head,
}


BMP :: struct {
	header: ^V5Head,
	pixels: []u8,
}

loadBMP :: proc (file: string ) -> (bmp: BMP, ok: bool){
	img, fok := os.read_entire_file(file)
	if !fok do return {}, false
	rd := raw_data(img)
	bmp.header = make([^]V5Head, 1)
	header := transmute(^V5Head)rd
	bmp.header^ = header^
		if bmp.header.head != {'B', 'M'} {
			return {}, false
		}

	offset := bmp.header.pixelOffset
	size := bmp.header.imageSize
	data := img[offset : offset + size]

	stride := i32(math.ceil(f32(bmp.header.bitsPerPixel) * f32(bmp.header.width) / 32) * 4)
	pixels := make([]u32, bmp.header.width * bmp.header.height)
	for i in 0 ..< bmp.header.height {
		for j in 0 ..< bmp.header.width {
			pixelIndex := i*bmp.header.width + j
			index := i * stride + j
			dataIndex := (j*3) + (i*stride)
			pixels[pixelIndex] = 0xFF << 24 | u32(data[dataIndex+2]) << 16 | u32(data[dataIndex+1]) << 8 | u32(data[dataIndex])

		}
	}
	delete(img)
	bmp.pixels = transmute([]u8)pixels

	return bmp, true
}

deleteBMP :: proc (bmp: BMP){
	free(bmp.header)
	delete(bmp.pixels)
}
