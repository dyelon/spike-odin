package vk
import vk "vendor:vulkan"
import "core:fmt"
import "core:mem"
import "core:math"
import la "core:math/linalg"
import "core:math/bits"

import "../window"

Context :: struct {
	wctx: ^window.Context,
	instance: vk.Instance,
	debugMessenger: vk.DebugUtilsMessengerEXT,
	physicalDevice: vk.PhysicalDevice,
	device: vk.Device,
	graphicsQueue: vk.Queue,
	surface: vk.SurfaceKHR,
	presentQueue: vk.Queue,
	swapChain: vk.SwapchainKHR,
	swapChainImages: []vk.Image,
	swapChainImageFormat: vk.Format,
	swapChainExtent: vk.Extent2D,
	swapChainImageViews: []vk.ImageView,
	renderPass: vk.RenderPass,
	descriptorSetLayout: vk.DescriptorSetLayout,
	pipelineLayout: vk.PipelineLayout,
	graphicsPipeline: vk.Pipeline,
	swapChainFramebuffers: []vk.Framebuffer,
	commandPool: vk.CommandPool,
	commandBuffers: [MAX_FRAMES_IN_FLIGHT]vk.CommandBuffer,
	imageAvailableSemaphores: [MAX_FRAMES_IN_FLIGHT]vk.Semaphore,
	renderFinishedSemaphores: [MAX_FRAMES_IN_FLIGHT]vk.Semaphore,
	inFlightFences: [MAX_FRAMES_IN_FLIGHT]vk.Fence,
	vertexBuffer: vk.Buffer,
	vertexBufferMemory: vk.DeviceMemory,
	indexBuffer: vk.Buffer,
	indexBufferMemory: vk.DeviceMemory,
	uniformBuffers: []vk.Buffer,
	uniformBuffersMemory: []vk.DeviceMemory,
	uniformBuffersMapped: []rawptr,
	descriptorPool: vk.DescriptorPool,
	descriptorSets: [MAX_FRAMES_IN_FLIGHT]vk.DescriptorSet,
	currentFrame: u32,
	frameCount: u32,
	frameBufferResized: bool,
	textureImage : vk.Image,
	textureImageMemory : vk.DeviceMemory,
	textureImageView: vk.ImageView,
	textureSampler: vk.Sampler,
	depthImage : vk.Image,
	depthImageMemory : vk.DeviceMemory,
	depthImageView: vk.ImageView,
		skyboxTex: Texture,

}

Error :: enum {
    None,
	BufferCreationError,
	CommandBufferAllocationError,
	CommandBufferRecordingError,
	CommandPoolCreationError,
	DebugMessengerCreationFailed,
	DescriptorSetsAllocationError,
	DescriptorSetLayoutError,
	DescriptorPoolCreationError,
	FrameBufferCreationError,
	FunctionLoadError,
	ImageAcqusitionError,
	ImageLoadError,
	ImageViewCreationError,
	LibraryLoadError,
	LogicalDeviceCreationError,
	MemoryAllocationError,
	NoDeviceSupport,
	NoFormatFound,
	NoSuitableDeviceFound,
	NoSuitableMemoryError,
	PipelineCreationError,
	PipelineLayoutCreationError,
	QueueSubmitError,
	QueuePresentError,
	RenderPassCreationError,
	RequestedValidationLayersNotAvailable,
	RequiredLayerNotAvailable,
	SamplerCreationError,
	ShaderLoadError,
	ShaderModuleCreationError,
	SurfaceCreationError,
	SwapChainCreationError,
	SyncObjectCreationError,
	TextureCreationError,
	UnsupportedTransition,
	VertexBufferCreationError,
}

Vertex :: struct {
	pos: [3]f32,
	color: [3]f32,
	texCoord: [2]f32,
}

Texture :: struct {
		image: vk.Image,
		memory: vk.DeviceMemory,
		view: vk.ImageView,
		sampler: vk.Sampler,
}

getBindingDescription :: proc() -> vk.VertexInputBindingDescription {
	res := vk.VertexInputBindingDescription {
		binding = 0,
		stride = size_of(Vertex),
		inputRate = .VERTEX,
	}
	return res
}

Camera :: struct {
	pos: la.Vector3f32,
	front: la.Vector3f32,
	up: la.Vector3f32,
	pitch: f32,
	yaw: f32,
}

initCamera :: proc() -> Camera {
	return Camera{
		pos = {0, 0, -2},
		front = {0, 0, -1},
		up = {0, -1, 0},
		pitch = 0,
		yaw = math.PI/2,
	}
}

getAttributeDescriptions :: proc() -> (res: [3]vk.VertexInputAttributeDescription) {
	return {
		{
			binding = 0,
			location = 0,
			format = .R32G32B32_SFLOAT,
			offset = u32(offset_of(Vertex, pos))
		},
		{
			binding = 0,
			location = 1,
			format = .R32G32B32_SFLOAT,
			offset = u32(offset_of(Vertex, color))
		},
		{
			binding = 0,
			location = 2,
			format = .R32G32_SFLOAT,
			offset = u32(offset_of(Vertex, texCoord))
		}
	}
}

UniformBufferObject :: struct {
	model: la.Matrix4f32,
	view: la.Matrix4f32,
	proj: la.Matrix4f32,
}

updateUniformBuffer :: proc(ctx: ^Context, camera: ^Camera) {
	//TODO Timer
	using la
	ident : Matrix4f32 = 1
	camXAxis : f32 = cos(camera.yaw) * cos(camera.pitch)
	camYAxis : f32 = sin(camera.pitch)
	camZAxis : f32 = sin(camera.yaw) * cos(camera.pitch)
	camera.front = normalize(Vector3f32{camXAxis, camYAxis, camZAxis})
	aspect : f32 = f32(ctx.swapChainExtent.width)/f32(ctx.swapChainExtent.height)
	ubo := UniformBufferObject{
		model = ident,
		view = matrix4_look_at(camera.pos, (camera.pos + camera.front), camera.up),
		proj = matrix4_infinite_perspective_f32(math.PI/4, aspect, 0.1),
	}

	mem.copy(ctx.uniformBuffersMapped[ctx.currentFrame], &ubo, size_of(UniformBufferObject))

}

ROOT3 :: 1.7320508075688772935274463415058723669428052538103806280558069794

BS :: 50

vertices := [?]Vertex {
	//Red
	Vertex{{-0.5, -0.5, 0}, {0, 0, 1}, {0, 1}},
	Vertex{{ 0.5, -0.5, 0}, {1, 0, 0}, {1, 1}},
	Vertex{{ 0.5,  0.5, 0}, {0, 1, 0}, {1, 0}},
	Vertex{{-0.5,  0.5, 0}, {1, 1, 1}, {0, 0}},

	Vertex{{-0.5, -0.5, 0.5}, {0, 0, 1}, {0, 1}},
	Vertex{{ 0.5, -0.5, 0.5}, {1, 0, 0}, {1, 1}},
	Vertex{{ 0.5,  0.5, 0.5}, {0, 1, 0}, {1, 0}},
	Vertex{{-0.5,  0.5, 0.5}, {1, 1, 1}, {0, 0}},

}
indices := [?]u16 {
	0, 1, 2,
	2, 3, 0,

	4,5,6,6,7,4,
}

offset : f32 = 0

Chunk :: struct {
	pos: la.Vector2f32,
	size: f32,
}

//Vulkan gloabal Config
@(private)
validationLayers := [?]cstring{"VK_LAYER_KHRONOS_validation"}
@(private)
deviceExtensions := [?]cstring{vk.KHR_SWAPCHAIN_EXTENSION_NAME}

DEFAULT_PRESENT_MODE : vk.PresentModeKHR : .MAILBOX

MAX_FRAMES_IN_FLIGHT :: 2

init :: proc (name: cstring, wctx: ^window.Context) -> (ctx: Context, err: Error) {
	ctx.wctx = wctx
	loadVulkan(&ctx) or_return

	createInstance(&ctx, name) or_return
	setupDebugMessenger(&ctx) or_return
	createSurface(&ctx) or_return
	pickPhysicalDevice(&ctx) or_return
	createLogicalDevice(&ctx) or_return
	createSwapChain(&ctx) or_return
	createImageViews(&ctx) or_return
	createRenderPass(&ctx) or_return
	createDescriptorSetLayout(&ctx) or_return
	createGraphicsPipeline(&ctx) or_return
	createCommandPool(&ctx) or_return
	createDepthResources(&ctx) or_return
	createFramebuffers(&ctx) or_return
	createTextureImage(&ctx) or_return
	createTextureImageView(&ctx) or_return
	createTextureSampler(&ctx) or_return
	createVertexBuffer(&ctx) or_return
	createIndexBuffer(&ctx) or_return
	createUniformBuffers(&ctx) or_return
	createDescriptorPool(&ctx) or_return
	createDescriptorSets(&ctx) or_return
	createCommandBuffers(&ctx) or_return
	createSyncObjects(&ctx) or_return
	return ctx, nil
}

drawFrame :: proc(ctx: ^Context, camera: ^Camera) -> Error {
	vk.WaitForFences(ctx.device, 1, &ctx.inFlightFences[ctx.currentFrame], true, bits.U64_MAX)
	imageIndex : u32

	res := vk.AcquireNextImageKHR(ctx.device, ctx.swapChain, bits.U64_MAX, ctx.imageAvailableSemaphores[ctx.currentFrame], {}, &imageIndex)


	if res == .ERROR_OUT_OF_DATE_KHR {
		recreateSwapChain(ctx)
		return nil
	}
	else if res != .SUCCESS && res != .SUBOPTIMAL_KHR{
		return .ImageAcqusitionError
	}

	vk.ResetFences(ctx.device, 1, &ctx.inFlightFences[ctx.currentFrame])

	updateUniformBuffer(ctx, camera)

	vk.ResetCommandBuffer(ctx.commandBuffers[ctx.currentFrame], {})

	recordCommandBuffer(ctx, ctx.commandBuffers[ctx.currentFrame], imageIndex)
	waitStages := [?]vk.PipelineStageFlags{{.COLOR_ATTACHMENT_OUTPUT}}
	submitInfo := vk.SubmitInfo{
		sType = .SUBMIT_INFO,
		waitSemaphoreCount = 1,
		pWaitSemaphores = &ctx.imageAvailableSemaphores[ctx.currentFrame],
		pWaitDstStageMask = &waitStages[0],
		commandBufferCount = 1,
		pCommandBuffers = &ctx.commandBuffers[ctx.currentFrame],
		signalSemaphoreCount = 1,
		pSignalSemaphores = &ctx.renderFinishedSemaphores[ctx.currentFrame],
	}

	if vk.QueueSubmit(ctx.graphicsQueue, 1, &submitInfo, ctx.inFlightFences[ctx.currentFrame]) != .SUCCESS{
		return .QueueSubmitError
	}

	presentInfo := vk.PresentInfoKHR{
		sType = .PRESENT_INFO_KHR,
		waitSemaphoreCount = 1,
		pWaitSemaphores = &ctx.renderFinishedSemaphores[ctx.currentFrame],
		swapchainCount = 1,
		pSwapchains = &ctx.swapChain,
		pImageIndices = &imageIndex,
		pResults = nil,
	}

	res = vk.QueuePresentKHR(ctx.presentQueue, &presentInfo)

	bufferResized := ctx.swapChainExtent.width != ctx.wctx.width || ctx.swapChainExtent.height != ctx.wctx.height

	if res == .ERROR_OUT_OF_DATE_KHR || res == .SUBOPTIMAL_KHR || bufferResized {
		recreateSwapChain(ctx)
	}
	else if res != .SUCCESS {
		return .QueuePresentError
	}

	ctx.currentFrame = (ctx.currentFrame + 1) % MAX_FRAMES_IN_FLIGHT
	ctx.frameCount += 1

	return nil
}

deinit :: proc (ctx: ^Context) {
	vk.DeviceWaitIdle(ctx.device)
	when ODIN_DEBUG {
		defer fmt.eprintf("Vulkan cleanup finished\n")
		vk.DestroyDebugUtilsMessengerEXT(ctx.instance, ctx.debugMessenger, nil)
	}
	for i in 0..<MAX_FRAMES_IN_FLIGHT{
		vk.DestroySemaphore(ctx.device, ctx.imageAvailableSemaphores[i], nil)
		vk.DestroySemaphore(ctx.device, ctx.renderFinishedSemaphores[i], nil)
		vk.DestroyFence(ctx.device, ctx.inFlightFences[i], nil)

		vk.DestroyBuffer(ctx.device, ctx.uniformBuffers[i], nil)
		vk.FreeMemory(ctx.device, ctx.uniformBuffersMemory[i], nil)
	}
	cleanupSwapChain(ctx)
	vk.DestroySampler(ctx.device, ctx.textureSampler, nil)
	vk.DestroyImage(ctx.device, ctx.textureImage, nil)
	vk.DestroyImageView(ctx.device, ctx.textureImageView, nil)
	vk.FreeMemory(ctx.device, ctx.textureImageMemory, nil)
	vk.DestroyDescriptorPool(ctx.device, ctx.descriptorPool, nil)
	vk.DestroyDescriptorSetLayout(ctx.device, ctx.descriptorSetLayout, nil)
	vk.DestroyBuffer(ctx.device, ctx.indexBuffer, nil)
	vk.FreeMemory(ctx.device, ctx.indexBufferMemory, nil)
	vk.DestroyBuffer(ctx.device, ctx.vertexBuffer, nil)
	vk.FreeMemory(ctx.device, ctx.vertexBufferMemory, nil)
	vk.DestroyCommandPool(ctx.device, ctx.commandPool, nil)
	vk.DestroyRenderPass(ctx.device, ctx.renderPass, nil)
	vk.DestroyPipeline(ctx.device, ctx.graphicsPipeline, nil)
	vk.DestroyPipelineLayout(ctx.device, ctx.pipelineLayout, nil)
	vk.DestroyDevice(ctx.device, nil)
	vk.DestroySurfaceKHR(ctx.instance, ctx.surface, nil)
	vk.DestroyInstance(ctx.instance, nil)
}
