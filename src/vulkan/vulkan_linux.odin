//+private
package vk
import vk "vendor:vulkan"
import "core:dynlib"
import "vendor:x11/xlib"
import "../window"
import "core:math"
import "core:os"
import "core:math/bits"

ProcCreateXlibSurfaceKHR :: #type proc "system" (instance: vk.Instance, pCreateInfo: ^XlibSurfaceCreateInfoKHR, pAllocator: ^vk.AllocationCallbacks, pSurface: ^vk.SurfaceKHR) -> vk.Result
CreateXlibSurfaceKHR : ProcCreateXlibSurfaceKHR
XlibSurfaceCreateFlagsKHR :: distinct bit_set[XlibSurfaceCreateFlagKHR; vk.Flags]
XlibSurfaceCreateFlagKHR :: enum vk.Flags {
}
XlibSurfaceCreateInfoKHR :: struct {
	sType: vk.StructureType,
	pNext: rawptr,
	flags: XlibSurfaceCreateFlagsKHR,
	dpy: ^xlib.Display,
	window: xlib.Window
}

GLFW :: #config(GLFW, false)

requiredExtensions := [?]cstring{
	"VK_KHR_surface",
	"VK_KHR_xlib_surface"
}

loadVulkan :: proc (ctx: ^Context) -> Error {
	libptr, ok := dynlib.load_library("/usr/lib64/libvulkan.so.1")
	if !ok {
		return .LibraryLoadError
	}
	getProcAddressFnPtr, ok2 := dynlib.symbol_address(libptr, "vkGetInstanceProcAddr")
	if !ok2 {
		return .FunctionLoadError
	}
	vk.load_proc_addresses(getProcAddressFnPtr) //Loader procs
	return nil
}

getRequiredExtensions :: proc() -> (extensions: []cstring) {
	when GLFW {
		return glfw.GetRequiredExtensions()
	} else {
		return requiredExtensions[:]
	}
}

createSurface :: proc (ctx: ^Context) -> Error {
	//TODO GLFW
	CreateXlibSurfaceKHR = auto_cast vk.GetInstanceProcAddr(ctx.instance, "vkCreateXlibSurfaceKHR")
	xlibSurfaceCreateInfo := XlibSurfaceCreateInfoKHR{
		sType = .XLIB_SURFACE_CREATE_INFO_KHR,
		dpy = ctx.wctx.dpy,
		window = ctx.wctx.win,
	}
	if CreateXlibSurfaceKHR(ctx.instance, &xlibSurfaceCreateInfo, nil, &ctx.surface) != .SUCCESS {
		return .SurfaceCreationError
	}
	return nil
}

chooseSwapExtent :: proc (capabilities: ^vk.SurfaceCapabilitiesKHR, ctx: ^Context) -> vk.Extent2D {
	if capabilities.currentExtent.width != bits.U32_MAX{
		return capabilities.currentExtent
	} else {
		width, height : u32
		when GLFW {
			glfw.GetFramebufferSize(wctx.win, &width, &height)
		} else {
			rootReturn : xlib.Window
			xret, yret : i32
			border : u32
			depth : u32
			xlib.XGetGeometry(ctx.wctx.dpy, ctx.wctx.win, &rootReturn, &xret, &yret, &width, &height, &border, &depth)
		}
		width = clamp(width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width)
		height = clamp(height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height)

		return vk.Extent2D{
			width = width,
			height = height
		}
	}
}

//NOTE This may want to go in the platform layer instead
loadShader :: proc (file: string) -> (data: []u32, success: bool){
	res, ok := os.read_entire_file_from_filename(file)
	return transmute([]u32)res, ok
}

loadFile :: proc (file: string) -> (data: []u8, success: bool){
		return os.read_entire_file(file)
}
