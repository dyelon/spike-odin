//+private
package vk

import vk "vendor:vulkan"
import "core:strings"
import "core:math"
import "core:fmt"
import "core:image"
import "core:image/png"
import "core:mem"
import "base:runtime"
import "../util"

foreign import stb_image "../../bin/stb_image.a"
foreign stb_image {
	stbi_load :: proc "c" (filename: cstring, width, height, channels: ^i32, desired_channesl: i32) -> rawptr ---
}

QueueFamilyName :: enum{Graphics, Present}
QueueFamilyIndices :: struct {
	using _: struct #raw_union {
		indexArray: [QueueFamilyName]Maybe(u32),
		using _: struct {
			Graphics: Maybe(u32),
			Present: Maybe(u32),
		},
	},
	familyCount: u32,
}

SwapChainSupportDetails :: struct {
	capabilities: vk.SurfaceCapabilitiesKHR,
	formats: []vk.SurfaceFormatKHR,
	presentModes: []vk.PresentModeKHR,
}

isComplete :: proc (qfi: QueueFamilyIndices) -> bool {
    complete := true
    for index in qfi.indexArray{
		if index == nil {
			complete = false
			break
		}
	}
    return complete
}

createInstance :: proc (ctx: ^Context, name: cstring) -> Error {
    appinfo: vk.ApplicationInfo = {}
    appinfo.sType = .APPLICATION_INFO
    appinfo.pApplicationName = name
    appinfo.applicationVersion = vk.MAKE_VERSION(1, 0, 0)
    appinfo.pEngineName = "No Engine"
    appinfo.engineVersion = vk.MAKE_VERSION(1, 0, 0)
    appinfo.apiVersion = vk.API_VERSION_1_3; //A good idea to be greater than 1.1
	//so that vkGetInstanceProcAddr can load itself
    createinfo: vk.InstanceCreateInfo = {}
    createinfo.sType = .INSTANCE_CREATE_INFO
    createinfo.pNext = nil
    createinfo.flags = {}
    createinfo.pApplicationInfo = &appinfo

	counter: u32

	//TODO Clean this up some
	required := getRequiredExtensions()
	names := make([dynamic]cstring, 0, len(required)+1)
	append(&names, ..required[:])

	//TODO Try this again
	extns : [^]vk.ExtensionProperties = nil;
	for i in 0..<2{
		if i == 1 do extns = make([^]vk.ExtensionProperties, counter)
		for res := vk.Result.INCOMPLETE; res == .INCOMPLETE; {
			res = vk.EnumerateInstanceExtensionProperties(nil, &counter, extns)
		}
	}

	//Maybe pull this out into a function
	for name in names {
		found := false
		for i in 0..<counter {
			if strings.compare(string(cstring(raw_data(&extns[i].extensionName))), string(name)) == 0 {
				found = true
				break
			}
		}
		if !found do return .RequiredLayerNotAvailable
	}


	when ODIN_DEBUG {
		debugCreateinfo : vk.DebugUtilsMessengerCreateInfoEXT = {}
		debugCreateinfo.sType = .DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT
        debugCreateinfo.messageSeverity = {
				.VERBOSE,
				.INFO,
				.WARNING,
				.ERROR,
		}
		debugCreateinfo.messageType = {
				.GENERAL,
				.VALIDATION,
				.PERFORMANCE,
		}
		debugCreateinfo.pfnUserCallback = debugCallback;
		debugCreateinfo.pUserData = nil;
		debugCreateinfo.pNext = nil;
		debugCreateinfo.flags = {};

		debugCreateinfo.pNext = &debugCreateinfo

		if !validationLayersAvailable() {
			fmt.eprintf("Validation layers requested, but not available")
			return .RequestedValidationLayersNotAvailable
		}
		createinfo.enabledLayerCount = len(validationLayers)
		createinfo.ppEnabledLayerNames = raw_data(&validationLayers)

		append(&names, "VK_EXT_debug_utils")
	}

	createinfo.enabledExtensionCount = u32(len(names))
	createinfo.ppEnabledExtensionNames = &names[0]

	fmt.printf("Enabled Extensions:\n")
	for name in names do fmt.printf("%s\n", name)

	res := vk.CreateInstance(&createinfo, nil, &ctx.instance)
	vk.load_proc_addresses(ctx.instance) //Instance Procs

	return nil
}

debugCallback :: proc "system" (severity: vk.DebugUtilsMessageSeverityFlagsEXT, messagetype: vk.DebugUtilsMessageTypeFlagsEXT, callbackdata: ^vk.DebugUtilsMessengerCallbackDataEXT, userdata: rawptr) -> b32 {
    context = runtime.default_context()
    sevstring : string
    switch severity {
	case {.VERBOSE}: sevstring = "VERBOSE"
	case {.INFO}: sevstring = "INFO"
	case {.WARNING}: sevstring = "WARN"
	case {.ERROR}: sevstring = "ERROR"
	case : sevstring = "OTHER"
	}

    requestedMessageTypes : vk.DebugUtilsMessageSeverityFlagsEXT = {//.VERBOSE,
		//.INFO,
			.WARNING,
			.ERROR}
    if severity <= requestedMessageTypes {
		fmt.eprintf("[%s] Validation layer: %s\n", sevstring, string(callbackdata.pMessage))
	}
    return false
}

validationLayersAvailable :: proc () -> bool {
    layerCount : u32
    vk.EnumerateInstanceLayerProperties(&layerCount, nil)
    layers := make([^]vk.LayerProperties, layerCount)
    vk.EnumerateInstanceLayerProperties(&layerCount, layers)
    for requestedLayer in validationLayers {
		found := false

		for i in 0..<layerCount {
			if (strings.compare(string(cstring(raw_data(&layers[i].layerName))), string(requestedLayer)) == 0){
				found = true
				break
			}
		}
		if !found do return false
	}
    return true
}

setupDebugMessenger :: proc (ctx: ^Context) -> Error {
    when ODIN_DEBUG {
		createinfo : vk.DebugUtilsMessengerCreateInfoEXT = {}
		createinfo.sType = .DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT
		createinfo.messageSeverity = {
				.VERBOSE,
				.INFO,
				.WARNING,
				.ERROR,
		}
		createinfo.messageType = {
				.GENERAL,
				.VALIDATION,
				.PERFORMANCE,
		}
		createinfo.pfnUserCallback = debugCallback;
		createinfo.pUserData = nil;
		createinfo.pNext = nil;
		createinfo.flags = {};

		if(vk.CreateDebugUtilsMessengerEXT(ctx.instance, &createinfo, nil, &ctx.debugMessenger) != .SUCCESS){
			return .DebugMessengerCreationFailed
		}
	}
    return nil
}

pickPhysicalDevice :: proc (ctx: ^Context) -> Error {
	ctx.physicalDevice = nil
	deviceCount : u32
	vk.EnumeratePhysicalDevices(ctx.instance, &deviceCount, nil)
	if deviceCount == 0 do return .NoDeviceSupport
	devices := make([^]vk.PhysicalDevice, deviceCount)
	vk.EnumeratePhysicalDevices(ctx.instance, &deviceCount, devices)
	for device in devices[:deviceCount] {
		if deviceIsSuitable(device, ctx) {
			ctx.physicalDevice = device
			break
		}
	}
	if ctx.physicalDevice == nil {
		return .NoSuitableDeviceFound
	}

    return nil
}

createLogicalDevice :: proc (ctx: ^Context) -> Error {
    indices := findQueueFamilies(ctx.physicalDevice, ctx.surface)

    usedFamilies := make(map[u32]bool)
    defer delete(usedFamilies)

    queueInfoArray := make([dynamic]vk.DeviceQueueCreateInfo, 0, indices.familyCount)
    for index in indices.indexArray {
		if index.? not_in usedFamilies{
			usedFamilies[index.?] = true
		}
	}

    queuePriority : f32 = 1
    for index in usedFamilies {
		queueCreateInfo := vk.DeviceQueueCreateInfo{
			sType = .DEVICE_QUEUE_CREATE_INFO,
			queueFamilyIndex = index,
			queueCount = 1,
			pQueuePriorities = &queuePriority,
		}
		append(&queueInfoArray, queueCreateInfo)
	}

    deviceFeatures : vk.PhysicalDeviceFeatures
	deviceFeatures.samplerAnisotropy = true

    createInfo : vk.DeviceCreateInfo
    createInfo.sType = .DEVICE_CREATE_INFO
    createInfo.pQueueCreateInfos = &queueInfoArray[0]
    createInfo.queueCreateInfoCount = u32(len(queueInfoArray))

    createInfo.pEnabledFeatures = &deviceFeatures

	createInfo.enabledExtensionCount = len(deviceExtensions)
	createInfo.ppEnabledExtensionNames = &deviceExtensions[0]

    when ODIN_DEBUG {
		createInfo.enabledLayerCount = len(validationLayers)
		createInfo.ppEnabledLayerNames = raw_data(&validationLayers)
	}

    if (vk.CreateDevice(ctx.physicalDevice, &createInfo, nil, &ctx.device) != .SUCCESS) {
		return .LogicalDeviceCreationError
	}
    vk.GetDeviceQueue(ctx.device, indices.Graphics.?, 0, &ctx.graphicsQueue)
    vk.GetDeviceQueue(ctx.device, indices.Present.?, 0, &ctx.presentQueue)
    return nil
}

createSwapChain :: proc (ctx: ^Context) -> Error {
    swapChainSupport := querySwapChainSupport(ctx.physicalDevice, ctx.surface)

    surfaceFormat := chooseSwapSurfaceFormat(swapChainSupport.formats)
    presentMode := chooseSwapPresentMode(swapChainSupport.presentModes)
    extent := chooseSwapExtent(&swapChainSupport.capabilities, ctx)

    imageCount := swapChainSupport.capabilities.minImageCount + 1

    if swapChainSupport.capabilities.maxImageCount > 0 &&
        imageCount > swapChainSupport.capabilities.maxImageCount {
			imageCount = swapChainSupport.capabilities.maxImageCount;
		}
    createinfo := vk.SwapchainCreateInfoKHR {
		sType = .SWAPCHAIN_CREATE_INFO_KHR,
		surface = ctx.surface,
		minImageCount = imageCount,
		imageFormat = surfaceFormat.format,
		imageColorSpace = surfaceFormat.colorSpace,
		imageExtent = extent,
		imageArrayLayers = 1,
		imageUsage = {.COLOR_ATTACHMENT},
	}

    indices := findQueueFamilies(ctx.physicalDevice, ctx.surface)
    indexArray := [?]u32{indices.Graphics.?, indices.Present.?}

    if indices.Graphics != indices.Present {
		createinfo.imageSharingMode = .CONCURRENT
		createinfo.queueFamilyIndexCount = 2
		createinfo.pQueueFamilyIndices = &indexArray[0]
	} else {
		createinfo.imageSharingMode = .EXCLUSIVE
	}
    createinfo.preTransform = swapChainSupport.capabilities.currentTransform
    createinfo.compositeAlpha = {.PRE_MULTIPLIED} //TODO Programatic selection
    createinfo.presentMode = presentMode
    createinfo.clipped = true
    createinfo.oldSwapchain = {}

    if vk.CreateSwapchainKHR(ctx.device, &createinfo, nil, &ctx.swapChain) != .SUCCESS {
		return .SwapChainCreationError
	}

    vk.GetSwapchainImagesKHR(ctx.device, ctx.swapChain, &imageCount, nil)
    ctx.swapChainImages = make([]vk.Image, imageCount)
    vk.GetSwapchainImagesKHR(ctx.device, ctx.swapChain, &imageCount, &ctx.swapChainImages[0])

    ctx.swapChainImageFormat = surfaceFormat.format
    ctx.swapChainExtent = extent

    return nil
}

createImageViews :: proc (ctx: ^Context) -> Error {
	//ctx.swapChainImageViews = make (len(ctx.swapChainImages))
	ctx.swapChainImageViews = make([]vk.ImageView, len(ctx.swapChainImages))

	for _, i in ctx.swapChainImages {
		ctx.swapChainImageViews[i] = createImageView(ctx, ctx.swapChainImages[i], ctx.swapChainImageFormat, {.COLOR}) or_return
	}
	return nil
}

createRenderPass :: proc (ctx: ^Context) -> Error {
    colorAttachment := vk.AttachmentDescription{
		format = ctx.swapChainImageFormat,
		samples = {._1},
		loadOp = .CLEAR,
		storeOp = .STORE,
		stencilLoadOp = .DONT_CARE,
		stencilStoreOp = .DONT_CARE,
		initialLayout = .UNDEFINED,
		finalLayout = .PRESENT_SRC_KHR,
	}

	depthAttachment := vk.AttachmentDescription {
		format = findDepthFormat(ctx) or_return,
		samples = {._1},
		loadOp = .CLEAR,
		storeOp = .DONT_CARE,
		stencilLoadOp = .DONT_CARE,
		stencilStoreOp = .DONT_CARE,
		initialLayout = .UNDEFINED,
		finalLayout = .DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
	}

    //NOTE I don't fully understand yet, but I think the 0 here is
    //the layout(location = 0) out vec4 outColor in the fragment shader
    colorAttachmentRef := vk.AttachmentReference{
		attachment = 0,
		layout = .COLOR_ATTACHMENT_OPTIMAL,
	}

    depthAttachmentRef := vk.AttachmentReference{
		attachment = 1,
		layout = .DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
	}

    dependency := vk.SubpassDependency{
		srcSubpass = vk.SUBPASS_EXTERNAL,
		dstSubpass = 0,
		srcStageMask = {.COLOR_ATTACHMENT_OUTPUT, .EARLY_FRAGMENT_TESTS},
		dstStageMask = {.COLOR_ATTACHMENT_OUTPUT, .EARLY_FRAGMENT_TESTS},
		dstAccessMask = {.COLOR_ATTACHMENT_WRITE, .DEPTH_STENCIL_ATTACHMENT_WRITE},
	}

    subpass := vk.SubpassDescription{
		pipelineBindPoint = .GRAPHICS,
		colorAttachmentCount = 1,
		pColorAttachments = &colorAttachmentRef,
		pDepthStencilAttachment = &depthAttachmentRef,
	}

	attachments := [?]vk.AttachmentDescription{colorAttachment, depthAttachment}

    createinfo := vk.RenderPassCreateInfo{
		sType = .RENDER_PASS_CREATE_INFO,
		attachmentCount = len(attachments),
		pAttachments = &attachments[0],
		subpassCount = 1,
		pSubpasses = &subpass,
		dependencyCount = 1,
		pDependencies = &dependency,
	}

    if vk.CreateRenderPass(ctx.device, &createinfo, nil, &ctx.renderPass) != .SUCCESS{
		return .RenderPassCreationError,
	}
    return nil
}

createDescriptorSetLayout :: proc (ctx: ^Context) -> Error {
	uboLayoutBinding := vk.DescriptorSetLayoutBinding {
		binding = 0,
		descriptorType = .UNIFORM_BUFFER,
		descriptorCount = 1,
		stageFlags = {.VERTEX},
		pImmutableSamplers = nil,
	}

	samplerLayoutBinding := vk.DescriptorSetLayoutBinding {
		binding = 1,
		descriptorCount = 1,
		descriptorType = .COMBINED_IMAGE_SAMPLER,
		pImmutableSamplers = nil,
		stageFlags = {.FRAGMENT}
	}

	bindings := [?]vk.DescriptorSetLayoutBinding{uboLayoutBinding, samplerLayoutBinding}

	layoutInfo := vk.DescriptorSetLayoutCreateInfo{
		sType = .DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
		bindingCount = len(bindings),
		pBindings = &bindings[0],
	}

	if vk.CreateDescriptorSetLayout(ctx.device, &layoutInfo, nil, &ctx.descriptorSetLayout) != .SUCCESS{
		return .DescriptorSetLayoutError
	}
	return nil
}

createGraphicsPipeline :: proc (ctx: ^Context) -> Error {
    vert, vok := loadShader("bin/vert.spv")
    frag, fok := loadShader("bin/frag.spv")
    if !vok || !fok do return .ShaderLoadError

    vertShaderModule := createShaderModule(ctx, vert) or_return
    fragShaderModule := createShaderModule(ctx, frag) or_return

    vertStageInfo := vk.PipelineShaderStageCreateInfo{
		sType = .PIPELINE_SHADER_STAGE_CREATE_INFO,
		stage = {.VERTEX},
		module = vertShaderModule,
		pName = "main",
	}
    fragStageInfo := vk.PipelineShaderStageCreateInfo{
		sType = .PIPELINE_SHADER_STAGE_CREATE_INFO,
		stage = {.FRAGMENT},
		module = fragShaderModule,
		pName = "main",
	}
    shaderStages := [?]vk.PipelineShaderStageCreateInfo{vertStageInfo, fragStageInfo}

    dynamicStates := [?]vk.DynamicState{.VIEWPORT, .SCISSOR}
    dynamicState := vk.PipelineDynamicStateCreateInfo{
		sType = .PIPELINE_DYNAMIC_STATE_CREATE_INFO,
		dynamicStateCount = len(dynamicStates),
		pDynamicStates = &dynamicStates[0],
	}

    bindingDesc := getBindingDescription()
    attrDescs := getAttributeDescriptions()

    vertexInputInfo := vk.PipelineVertexInputStateCreateInfo{
		sType = .PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
		vertexBindingDescriptionCount = 1,
		vertexAttributeDescriptionCount = len(attrDescs),
		pVertexBindingDescriptions = &bindingDesc,
		pVertexAttributeDescriptions = &attrDescs[0],
	}

    inputAssembly := vk.PipelineInputAssemblyStateCreateInfo{
		sType = .PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
		topology = .TRIANGLE_LIST,
		primitiveRestartEnable = false,
	}

    viewPortState := vk.PipelineViewportStateCreateInfo{
		sType = .PIPELINE_VIEWPORT_STATE_CREATE_INFO,
		viewportCount = 1,
		scissorCount = 1,
	}


    rasterizer := vk.PipelineRasterizationStateCreateInfo{
		sType = .PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
		depthClampEnable = false,
		rasterizerDiscardEnable = false,
		polygonMode = .FILL,
		lineWidth = 1,
		cullMode = {.BACK},
		frontFace = .COUNTER_CLOCKWISE,
		depthBiasEnable = false,
	}

    multisampling := vk.PipelineMultisampleStateCreateInfo{
		sType = .PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
		sampleShadingEnable = false,
		rasterizationSamples = {._1},
	}

    colorBlendAttachment := vk.PipelineColorBlendAttachmentState{
		colorWriteMask = {.R, .G, .B, .A},
		blendEnable = false,
	}

    colorBlending := vk.PipelineColorBlendStateCreateInfo{
		sType = .PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
		logicOpEnable = false,
		attachmentCount = 1,
		pAttachments = &colorBlendAttachment,
	}

	depthStencil := vk.PipelineDepthStencilStateCreateInfo {
		sType = .PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
		depthTestEnable = true,
		depthWriteEnable = true,
		depthCompareOp = .LESS,
		depthBoundsTestEnable = false,
	}

    pipelineLayoutInfo := vk.PipelineLayoutCreateInfo{
		sType = .PIPELINE_LAYOUT_CREATE_INFO,
		setLayoutCount = 1,
		pSetLayouts = &ctx.descriptorSetLayout,
	}

    if vk.CreatePipelineLayout(ctx.device, &pipelineLayoutInfo, nil, &ctx.pipelineLayout) != .SUCCESS{
		return .PipelineLayoutCreationError
	}

    pipelineInfo := vk.GraphicsPipelineCreateInfo{
		sType = .GRAPHICS_PIPELINE_CREATE_INFO,
		stageCount = 2,
		pStages = &shaderStages[0],
		pVertexInputState = &vertexInputInfo,
		pInputAssemblyState = &inputAssembly,
		pViewportState = &viewPortState,
		pRasterizationState = &rasterizer,
		pMultisampleState = &multisampling,
		pDepthStencilState = &depthStencil,
		pColorBlendState = &colorBlending,
		pDynamicState = &dynamicState,
		layout = ctx.pipelineLayout,
		renderPass = ctx.renderPass,
		subpass = 0,
		basePipelineHandle = {},
		basePipelineIndex = -1,
	}

    if vk.CreateGraphicsPipelines(ctx.device, {}, 1, &pipelineInfo, nil, &ctx.graphicsPipeline)  != .SUCCESS{
		return .PipelineCreationError,
	}

    vk.DestroyShaderModule(ctx.device, vertShaderModule, nil)
    vk.DestroyShaderModule(ctx.device, fragShaderModule, nil)
    return nil
}

createFramebuffers :: proc (ctx: ^Context) -> Error {
    ctx.swapChainFramebuffers = make([]vk.Framebuffer, len(ctx.swapChainImageViews))
    for _,i in ctx.swapChainImageViews {
		attachments := [?]vk.ImageView{
			ctx.swapChainImageViews[i],
			ctx.depthImageView,
		}

		framebufferInfo := vk.FramebufferCreateInfo{
			sType = .FRAMEBUFFER_CREATE_INFO,
			renderPass = ctx.renderPass,
			attachmentCount = len(attachments),
			pAttachments = &attachments[0],
			width = ctx.swapChainExtent.width,
			height = ctx.swapChainExtent.height,
			layers = 1,
		}

		if vk.CreateFramebuffer(ctx.device,
								&framebufferInfo,
								nil,
								&ctx.swapChainFramebuffers[i]) != .SUCCESS{
									return .FrameBufferCreationError
								}
	}
    return nil
}

createTextureImage :: proc (ctx: ^Context) -> Error {
    width, height, channels : i32
    pixels := stbi_load("assets/texture.bmp", &width, &height, &channels, 4)

    imageSize := vk.DeviceSize(width * height * 4)

    stagingBuffer : vk.Buffer
    stagingBufferMemory : vk.DeviceMemory

    createBuffer(ctx, imageSize, {.TRANSFER_SRC}, {.HOST_VISIBLE, .HOST_COHERENT}, &stagingBuffer, &stagingBufferMemory)

    data :  rawptr

	vk.MapMemory(ctx.device, stagingBufferMemory, 0, imageSize, {}, &data)
	mem.copy(data, pixels, int(imageSize))
	vk.UnmapMemory(ctx.device, stagingBufferMemory)

    createImage(ctx,
				u32(width),
				u32(height),
				.R8G8B8A8_SRGB,
				.LINEAR,
				{.TRANSFER_DST, .SAMPLED},
				{.DEVICE_LOCAL},
				&ctx.textureImage,
				&ctx.textureImageMemory,
			   )

	subres := vk.ImageSubresourceRange{
		baseMipLevel = 0,
		levelCount = 1,
		baseArrayLayer = 0,
		layerCount = 1,
	}
    transitionImageLayout(ctx, ctx.textureImage, .R8G8B8A8_SRGB, .UNDEFINED, .TRANSFER_DST_OPTIMAL, subres) or_return
	regions := []vk.BufferImageCopy{
		{
			bufferOffset = 0,
			bufferRowLength = 0,
			bufferImageHeight = 0,
			imageSubresource = {
				aspectMask = {.COLOR},
				mipLevel = 0,
				baseArrayLayer = 0,
				layerCount = 1,
			},
			imageOffset = {0, 0, 0},
			imageExtent = {
				u32(width), u32(height), 1,
			},
		},
	}
	copyBufferToImage(ctx, stagingBuffer, ctx.textureImage, regions)
	transitionImageLayout(ctx, ctx.textureImage, .R8G8B8A8_SRGB, .TRANSFER_DST_OPTIMAL, .SHADER_READ_ONLY_OPTIMAL, subres) or_return

    vk.DestroyBuffer(ctx.device, stagingBuffer, nil)
    vk.FreeMemory(ctx.device, stagingBufferMemory, nil)

    return nil
}

createTextureImageView :: proc (ctx: ^Context) -> Error {
	ctx.textureImageView = createImageView(ctx, ctx.textureImage, .R8G8B8A8_SRGB, {.COLOR}) or_return
	return nil
}

createTextureSampler :: proc (ctx: ^Context) -> Error {
    properties : vk.PhysicalDeviceProperties
    vk.GetPhysicalDeviceProperties(ctx.physicalDevice, &properties)
    samplerInfo := vk.SamplerCreateInfo{
		sType = .SAMPLER_CREATE_INFO,
		magFilter = .LINEAR,
		minFilter = .LINEAR,
		addressModeU = .MIRRORED_REPEAT, //For some reason we sample outside the texture
		addressModeV = .MIRRORED_REPEAT,
		addressModeW = .MIRRORED_REPEAT,
		anisotropyEnable = true,
		maxAnisotropy = properties.limits.maxSamplerAnisotropy,
		borderColor = .INT_OPAQUE_BLACK,
		unnormalizedCoordinates = false,
		compareEnable = false,
		compareOp = .ALWAYS,
		mipmapMode = .LINEAR,
		mipLodBias = 0,
		minLod = 0,
		maxLod = 0,
	}

    if vk.CreateSampler(ctx.device, &samplerInfo, nil, &ctx.textureSampler) != .SUCCESS {
		return .SamplerCreationError
	}

    return nil
}

createCubeMapTexture :: proc(ctx: ^Context) -> (result: Texture, err: Error) {
    width, height, channels : i32
	CubeDirection :: enum {
		Front,
		Back,
		Left,
		Right,
		Top,
		Bottom,
	}
	faces : [CubeDirection]rawptr
    faces[.Front] = stbi_load("assets/skyFront.png", &width, &height, &channels, 4)
    faces[.Back] = stbi_load("assets/skyBack.png", &width, &height, &channels, 4)
    faces[.Left] = stbi_load("assets/skyLeft.png", &width, &height, &channels, 4)
    faces[.Right] = stbi_load("assets/skyRight.png", &width, &height, &channels, 4)
    faces[.Top] = stbi_load("assets/skyTop.png", &width, &height, &channels, 4)
    faces[.Bottom] = stbi_load("assets/skyBottom.png", &width, &height, &channels, 4)

    imageSize := vk.DeviceSize(width * height * 4)

    stagingBuffer : vk.Buffer
    stagingBufferMemory : vk.DeviceMemory

    createBuffer(ctx, imageSize * len(faces), {.TRANSFER_SRC}, {.HOST_VISIBLE, .HOST_COHERENT}, &stagingBuffer, &stagingBufferMemory)

    data : rawptr

	vk.MapMemory(ctx.device, stagingBufferMemory, 0, imageSize, {}, &data)
	for face, i in faces{
		offset := uintptr(i) * uintptr(imageSize)
		w_ptr := uintptr(data)
		w_ptr += offset
		//w_ptr = mem.ptr_offset(w_ptr, offset)
		mem.copy(rawptr(w_ptr), face, int(imageSize))
	}
	vk.UnmapMemory(ctx.device, stagingBufferMemory)

    imageInfo := vk.ImageCreateInfo{
		sType = .IMAGE_CREATE_INFO,
		imageType = .D2,
		extent = vk.Extent3D{
			width = u32(width),
			height = u32(height),
			depth = 1,
		},
		mipLevels = 1,
		arrayLayers = 6,
		format = .R8G8B8_SRGB,
		tiling = .OPTIMAL,
		initialLayout = .UNDEFINED,
		usage = {.TRANSFER_DST, .SAMPLED},
		sharingMode = .EXCLUSIVE,
		samples = {._1},
		flags = {.CUBE_COMPATIBLE}
	}

    if vk.CreateImage(ctx.device, &imageInfo, nil, &result.image) != .SUCCESS{
		return {}, .TextureCreationError
	}

    memReqs : vk.MemoryRequirements
    vk.GetImageMemoryRequirements(ctx.device, result.image, &memReqs)

    memType := findMemoryType(ctx, memReqs.memoryTypeBits, {.HOST_VISIBLE, .HOST_COHERENT}) or_return
    allocInfo := vk.MemoryAllocateInfo{
		sType = .MEMORY_ALLOCATE_INFO,
		allocationSize = memReqs.size,
		memoryTypeIndex = memType,
	}

    if vk.AllocateMemory(ctx.device, &allocInfo, nil, &result.memory) != .SUCCESS {
		return {}, .MemoryAllocationError
	}

    vk.BindImageMemory(ctx.device, result.image, result.memory, 0)

	subres := vk.ImageSubresourceRange{
		baseMipLevel = 0,
		levelCount = 1,
		baseArrayLayer = 0,
		layerCount = 6,
	}
    transitionImageLayout(ctx, ctx.textureImage, .R8G8B8A8_SRGB, .UNDEFINED, .TRANSFER_DST_OPTIMAL, subres) or_return
	regions : [6]vk.BufferImageCopy
	for _,i in regions{
		regions[i] = {
			imageSubresource = {
				aspectMask = {.COLOR},
				mipLevel = 0,
				baseArrayLayer = u32(i),
				layerCount = 1,
			},
			imageExtent = {
				width = u32(width),
				height = u32(height),
				depth = 1,
			},
			bufferOffset = vk.DeviceSize(i)*imageSize,
		}
	}
	copyBufferToImage(ctx, stagingBuffer, ctx.textureImage, regions[:])
	transitionImageLayout(ctx, ctx.textureImage, .R8G8B8A8_SRGB, .TRANSFER_DST_OPTIMAL, .SHADER_READ_ONLY_OPTIMAL, subres) or_return

	vk.DestroyBuffer(ctx.device, stagingBuffer, nil)
	vk.FreeMemory(ctx.device, stagingBufferMemory, nil)
	return result, nil
}

createVertexBuffer :: proc (ctx: ^Context) -> Error {
    bufferSize : vk.DeviceSize = size_of(Vertex) * len(vertices)

    stagingBuffer : vk.Buffer
    stagingBufferMemory : vk.DeviceMemory

    createBuffer(ctx,
				 bufferSize,
				 {.TRANSFER_SRC},
				 {.HOST_VISIBLE, .HOST_COHERENT},
				 &stagingBuffer,
				 &stagingBufferMemory) or_return

    data : rawptr
    vk.MapMemory(ctx.device, stagingBufferMemory, 0, bufferSize, {}, &data)
    mem.copy(data, &vertices[0], int(bufferSize))
    vk.UnmapMemory(ctx.device, stagingBufferMemory)

    createBuffer(ctx,
				 bufferSize,
				 {.TRANSFER_DST, .VERTEX_BUFFER},
				 {.DEVICE_LOCAL},
				 &ctx.vertexBuffer,
				 &ctx.vertexBufferMemory) or_return

    copyBuffer(ctx, stagingBuffer, ctx.vertexBuffer, bufferSize)

    vk.DestroyBuffer(ctx.device, stagingBuffer, nil)
    vk.FreeMemory(ctx.device, stagingBufferMemory, nil)

    return nil
}

createIndexBuffer :: proc (ctx: ^Context) -> Error {
    bufferSize : vk.DeviceSize = size_of(type_of(indices[0])) * len(indices)

    stagingBuffer : vk.Buffer
    stagingBufferMemory : vk.DeviceMemory

    createBuffer(ctx,
				 bufferSize,
				 {.TRANSFER_SRC},
				 {.HOST_VISIBLE, .HOST_COHERENT},
				 &stagingBuffer,
				 &stagingBufferMemory) or_return

    data : rawptr
    vk.MapMemory(ctx.device, stagingBufferMemory, 0, bufferSize, {}, &data)
    mem.copy(data, &indices[0], int(bufferSize))
    vk.UnmapMemory(ctx.device, stagingBufferMemory)

    createBuffer(ctx,
				 bufferSize,
				 {.TRANSFER_DST, .INDEX_BUFFER},
				 {.DEVICE_LOCAL},
				 &ctx.indexBuffer,
				 &ctx.indexBufferMemory) or_return

    copyBuffer(ctx, stagingBuffer, ctx.indexBuffer, bufferSize)

    vk.DestroyBuffer(ctx.device, stagingBuffer, nil)
    vk.FreeMemory(ctx.device, stagingBufferMemory, nil)
    return nil
}

createUniformBuffers :: proc (ctx: ^Context) -> Error {
    bufferSize : vk.DeviceSize = size_of(UniformBufferObject)

    ctx.uniformBuffers = make([]vk.Buffer, MAX_FRAMES_IN_FLIGHT)
    ctx.uniformBuffersMemory = make([]vk.DeviceMemory, MAX_FRAMES_IN_FLIGHT)
    ctx.uniformBuffersMapped = make([]rawptr, MAX_FRAMES_IN_FLIGHT)

    for i in 0..<MAX_FRAMES_IN_FLIGHT {
		createBuffer(ctx,
					 bufferSize,
					 {.UNIFORM_BUFFER},
					 {.HOST_VISIBLE, .HOST_COHERENT},
					 &ctx.uniformBuffers[i],
					 &ctx.uniformBuffersMemory[i])
		vk.MapMemory(ctx.device, ctx.uniformBuffersMemory[i], 0, bufferSize, {}, &ctx.uniformBuffersMapped[i])
	}
    return nil
}

createDescriptorPool :: proc (ctx: ^Context) -> Error {
    poolSizes := [?]vk.DescriptorPoolSize{
			 {
				 type = .UNIFORM_BUFFER,
				 descriptorCount = MAX_FRAMES_IN_FLIGHT,},
			 {
				 type = .COMBINED_IMAGE_SAMPLER,
				 descriptorCount = MAX_FRAMES_IN_FLIGHT,
		},
	}

    poolInfo := vk.DescriptorPoolCreateInfo{
			 sType = .DESCRIPTOR_POOL_CREATE_INFO,
			 poolSizeCount = len(poolSizes),
			 pPoolSizes = &poolSizes[0],
			 maxSets = MAX_FRAMES_IN_FLIGHT,
	}

    if vk.CreateDescriptorPool(ctx.device, &poolInfo, nil, &ctx.descriptorPool) != .SUCCESS {
			 return .DescriptorPoolCreationError
	}
    return nil
}

createDescriptorSets :: proc (ctx: ^Context) -> Error {
    layouts : [MAX_FRAMES_IN_FLIGHT]vk.DescriptorSetLayout
    for _, i in layouts do layouts[i] = ctx.descriptorSetLayout
    allocInfo := vk.DescriptorSetAllocateInfo{
		sType = .DESCRIPTOR_SET_ALLOCATE_INFO,
		descriptorPool = ctx.descriptorPool,
		descriptorSetCount = MAX_FRAMES_IN_FLIGHT,
		pSetLayouts = &layouts[0]
	}

    if vk.AllocateDescriptorSets(ctx.device, &allocInfo, &ctx.descriptorSets[0]) != .SUCCESS{
		return .DescriptorSetsAllocationError
	}

	for i in 0..<MAX_FRAMES_IN_FLIGHT{
		bufferInfo := vk.DescriptorBufferInfo{
			buffer = ctx.uniformBuffers[i],
			offset = 0,
			range = size_of(UniformBufferObject),
		}

		imageInfo := vk.DescriptorImageInfo{
			imageLayout = .SHADER_READ_ONLY_OPTIMAL,
			imageView = ctx.textureImageView,
			sampler = ctx.textureSampler,
		}

		descriptorWrites := [?]vk.WriteDescriptorSet{
			{
				sType = .WRITE_DESCRIPTOR_SET,
				dstSet = ctx.descriptorSets[i],
				dstBinding = 0,
				dstArrayElement = 0,
				descriptorType = .UNIFORM_BUFFER,
				descriptorCount = 1,
				pBufferInfo = &bufferInfo,
			},
			{
				sType = .WRITE_DESCRIPTOR_SET,
				dstSet = ctx.descriptorSets[i],
				dstBinding = 1,
				dstArrayElement = 0,
				descriptorType = .COMBINED_IMAGE_SAMPLER,
				descriptorCount = 1,
				pImageInfo = &imageInfo,
			},
		}
		vk.UpdateDescriptorSets(ctx.device, len(descriptorWrites), &descriptorWrites[0], 0, nil)
	}


	return nil
}

createCommandPool :: proc (ctx: ^Context) -> Error {
    qfi := findQueueFamilies(ctx.physicalDevice, ctx.surface)
    poolinfo := vk.CommandPoolCreateInfo{
		sType = .COMMAND_POOL_CREATE_INFO,
		flags = {.RESET_COMMAND_BUFFER},
		queueFamilyIndex = qfi.Graphics.?,
	}

    if vk.CreateCommandPool(ctx.device, &poolinfo, nil, &ctx.commandPool) != .SUCCESS{
		return .CommandPoolCreationError,
	}
    return nil
}

createDepthResources :: proc (ctx: ^Context) -> Error {
    depthFormat := findDepthFormat(ctx) or_return

    createImage(ctx, ctx.swapChainExtent.width, ctx.swapChainExtent.height, depthFormat, .OPTIMAL,
				{.DEPTH_STENCIL_ATTACHMENT}, {.DEVICE_LOCAL}, &ctx.depthImage, &ctx.depthImageMemory)
    ctx.depthImageView = createImageView(ctx, ctx.depthImage, depthFormat, {.DEPTH}) or_return
	subres := vk.ImageSubresourceRange{
		baseMipLevel = 0,
		levelCount = 1,
		baseArrayLayer = 0,
		layerCount = 1,
	}
    transitionImageLayout(ctx, ctx.depthImage, depthFormat, .UNDEFINED, .DEPTH_STENCIL_ATTACHMENT_OPTIMAL, subres)

    return nil
}

createCommandBuffers :: proc (ctx: ^Context) -> Error {
    allocinfo := vk.CommandBufferAllocateInfo{
		sType = .COMMAND_BUFFER_ALLOCATE_INFO,
		commandPool = ctx.commandPool,
		level = .PRIMARY,
		commandBufferCount = len(ctx.commandBuffers),
	}

    if vk.AllocateCommandBuffers(ctx.device, &allocinfo, &ctx.commandBuffers[0]) != .SUCCESS{
		return .CommandBufferAllocationError
	}
    return nil
}

recordCommandBuffer :: proc (ctx: ^Context, cb: vk.CommandBuffer, imageIndex: u32) -> Error {
    begininfo := vk.CommandBufferBeginInfo{
		sType = .COMMAND_BUFFER_BEGIN_INFO,
	}

    if vk.BeginCommandBuffer(cb, &begininfo) != .SUCCESS{
		return .CommandBufferRecordingError
	}

    //clearColorValue := [4]f32{0, 0, 0, 1}
    clearColors := [?]vk.ClearValue{
		{
			color = {
				float32 = {0, 0, 0, 1},
			},
		},
		{
			depthStencil = {
				depth = 1,
				stencil = 0,
			},
		},
	}


    renderpassinfo := vk.RenderPassBeginInfo{
		sType = .RENDER_PASS_BEGIN_INFO,
		renderPass = ctx.renderPass,
		framebuffer = ctx.swapChainFramebuffers[imageIndex],
		renderArea = {
			offset = {0, 0},
			extent = ctx.swapChainExtent,
		},
		clearValueCount = len(clearColors),
		pClearValues = &clearColors[0],
	}

    vk.CmdBeginRenderPass(cb, &renderpassinfo, .INLINE)
    vk.CmdBindPipeline(cb, .GRAPHICS, ctx.graphicsPipeline)
    viewport := vk.Viewport{
		x = 0,
		y = 0,
		width = f32(ctx.swapChainExtent.width),
		height = f32(ctx.swapChainExtent.height),
		minDepth = 0,
		maxDepth = 1,
	}
	vk.CmdSetViewport(cb, 0, 1, &viewport)

    scissor := vk.Rect2D{
		offset = {0, 0},
		extent = ctx.swapChainExtent,
	}
    vk.CmdSetScissor(cb, 0, 1, &scissor)

	vertexBuffers := [?]vk.Buffer{ctx.vertexBuffer}
	offsets := [?]vk.DeviceSize{0}
	vk.CmdBindVertexBuffers(cb, 0, 1, &vertexBuffers[0], &offsets[0])

	vk.CmdBindIndexBuffer(cb, ctx.indexBuffer, 0, .UINT16)

	vk.CmdBindDescriptorSets(cb, .GRAPHICS, ctx.pipelineLayout, 0 ,1, &ctx.descriptorSets[ctx.currentFrame], 0, nil)

    vk.CmdDrawIndexed(cb, len(indices), 1, 0, 0, 0)
    vk.CmdEndRenderPass(cb)

    if vk.EndCommandBuffer(cb) != .SUCCESS{
		return .CommandBufferRecordingError
	}
    return nil
}

beginSingleTimeCommands :: proc (ctx: ^Context) -> vk.CommandBuffer{
    allocInfo := vk.CommandBufferAllocateInfo {
		sType = .COMMAND_BUFFER_ALLOCATE_INFO,
		level = .PRIMARY,
		commandPool = ctx.commandPool,
		commandBufferCount = 1,
	}

    commandBuffer : vk.CommandBuffer
    vk.AllocateCommandBuffers(ctx.device, &allocInfo, &commandBuffer)

    beginInfo := vk.CommandBufferBeginInfo{
		sType = .COMMAND_BUFFER_BEGIN_INFO,
		flags = {.ONE_TIME_SUBMIT}
	}
    vk.BeginCommandBuffer(commandBuffer, &beginInfo)

    return commandBuffer
}

endSingleTimeCommands :: proc (ctx: ^Context, commandBuffer: ^vk.CommandBuffer) {
    vk.EndCommandBuffer(commandBuffer^)

    submitInfo := vk.SubmitInfo{
		sType = .SUBMIT_INFO,
		commandBufferCount = 1,
		pCommandBuffers = commandBuffer,
	}

    vk.QueueSubmit(ctx.graphicsQueue, 1, &submitInfo, {})
    vk.QueueWaitIdle(ctx.graphicsQueue)

    vk.FreeCommandBuffers(ctx.device, ctx.commandPool, 1, commandBuffer)
}

transitionImageLayout :: proc (ctx: ^Context, image: vk.Image, format: vk.Format, oldLayout, newLayout: vk.ImageLayout, subres: vk.ImageSubresourceRange) -> Error {
    commandBuffer := beginSingleTimeCommands(ctx)

    sourceStage, destStage : vk.PipelineStageFlags

    barrier := vk.ImageMemoryBarrier{
		sType = .IMAGE_MEMORY_BARRIER,
		oldLayout = oldLayout,
		newLayout = newLayout,
		srcQueueFamilyIndex = vk.QUEUE_FAMILY_IGNORED,
		dstQueueFamilyIndex = vk.QUEUE_FAMILY_IGNORED,
		image = image,
		subresourceRange = subres,
		srcAccessMask = {},
		dstAccessMask = {},
	}

	if newLayout == .DEPTH_STENCIL_ATTACHMENT_OPTIMAL {
		barrier.subresourceRange.aspectMask = {.DEPTH}

		if hasStencilComponent(format) {
			barrier.subresourceRange.aspectMask += {.STENCIL}
		}
	} else {
		barrier.subresourceRange.aspectMask = {.COLOR}
	}

    if oldLayout == .UNDEFINED && newLayout == .TRANSFER_DST_OPTIMAL{
		barrier.srcAccessMask = {}
		barrier.dstAccessMask = {.TRANSFER_WRITE}

		sourceStage = {.TOP_OF_PIPE} //TODO Add VK_KHR_synchronization2 TOP OF PIPE is deprecated
		destStage = {.TRANSFER}
	} else if oldLayout == .TRANSFER_DST_OPTIMAL && newLayout == .SHADER_READ_ONLY_OPTIMAL{
		barrier.srcAccessMask = {.TRANSFER_WRITE}
		barrier.dstAccessMask = {.SHADER_READ}

		sourceStage = {.TRANSFER}
		destStage = {.FRAGMENT_SHADER}
	} else if oldLayout == .UNDEFINED && newLayout == .DEPTH_STENCIL_ATTACHMENT_OPTIMAL {
		barrier.srcAccessMask = {}
		barrier.dstAccessMask = {.DEPTH_STENCIL_ATTACHMENT_READ, .DEPTH_STENCIL_ATTACHMENT_WRITE}

		sourceStage = {.TOP_OF_PIPE}
		destStage = {.EARLY_FRAGMENT_TESTS}

	} else {
		return .UnsupportedTransition
	}

    vk.CmdPipelineBarrier(
		commandBuffer,
		sourceStage, destStage,
		{},
		0, nil,
		0, nil,
		1, &barrier
	)

    endSingleTimeCommands(ctx, &commandBuffer)
    return nil
}

copyBufferToImage :: proc (ctx: ^Context, buffer: vk.Buffer, image: vk.Image, regions: []vk.BufferImageCopy){
    commandBuffer := beginSingleTimeCommands(ctx)
    vk.CmdCopyBufferToImage(commandBuffer, buffer, image, .TRANSFER_DST_OPTIMAL, u32(len(regions)), &regions[0])
    endSingleTimeCommands(ctx, &commandBuffer)
}

createImage :: proc (ctx: ^Context, width, height: u32, format: vk.Format, tiling: vk.ImageTiling, usage: vk.ImageUsageFlags, properties: vk.MemoryPropertyFlags, image: ^vk.Image, imageMemory: ^vk.DeviceMemory) -> Error {
    imageInfo := vk.ImageCreateInfo{
		sType = .IMAGE_CREATE_INFO,
		imageType = .D2,
		extent = vk.Extent3D{
			width = width,
			height = height,
			depth = 1,
		},
		mipLevels = 1,
		arrayLayers = 1,
		format = format,
		tiling = tiling,
		initialLayout = .UNDEFINED,
		usage = usage,
		sharingMode = .EXCLUSIVE,
		samples = {._1},
	}

    if vk.CreateImage(ctx.device, &imageInfo, nil, image) != .SUCCESS{
		return .TextureCreationError
	}

    memReqs : vk.MemoryRequirements
    vk.GetImageMemoryRequirements(ctx.device, image^, &memReqs)

    memType := findMemoryType(ctx, memReqs.memoryTypeBits, properties) or_return
    allocInfo := vk.MemoryAllocateInfo{
		sType = .MEMORY_ALLOCATE_INFO,
		allocationSize = memReqs.size,
		memoryTypeIndex = memType,
	}

    if vk.AllocateMemory(ctx.device, &allocInfo, nil, imageMemory) != .SUCCESS {
		return .MemoryAllocationError
	}

    vk.BindImageMemory(ctx.device, image^, imageMemory^, 0)

    return nil
}

createImageView :: proc (ctx: ^Context, image: vk.Image, format: vk.Format, aspectFlags: vk.ImageAspectFlags) -> (view: vk.ImageView, err: Error) {
	viewInfo := vk.ImageViewCreateInfo{
        sType = .IMAGE_VIEW_CREATE_INFO,
        image = image,
        viewType = .D2,
        format = format,
        subresourceRange = {
			aspectMask = aspectFlags,
			baseMipLevel = 0,
			levelCount = 1,
			baseArrayLayer = 0,
			layerCount = 1,
		},
    }

	if vk.CreateImageView(ctx.device, &viewInfo, nil, &view) != .SUCCESS {
		return {}, .ImageViewCreationError
	}
	return
}

createBuffer :: proc (ctx: ^Context, size: vk.DeviceSize, usage: vk.BufferUsageFlags, properties: vk.MemoryPropertyFlags, buffer: ^vk.Buffer, bufferMemory: ^vk.DeviceMemory) -> Error {
    bufferInfo := vk.BufferCreateInfo{
		sType = .BUFFER_CREATE_INFO,
		size = size,
		usage = usage,
		sharingMode = .EXCLUSIVE,
	}

    if vk.CreateBuffer(ctx.device, &bufferInfo, nil, buffer) != .SUCCESS {
		return .BufferCreationError
	}
    memReqs : vk.MemoryRequirements
    vk.GetBufferMemoryRequirements(ctx.device, buffer^, &memReqs)

    memType := findMemoryType(ctx, memReqs.memoryTypeBits, properties) or_return
    allocInfo := vk.MemoryAllocateInfo {
		sType = .MEMORY_ALLOCATE_INFO,
		allocationSize = memReqs.size,
		memoryTypeIndex = memType,
	}

    if vk.AllocateMemory(ctx.device, &allocInfo, nil, bufferMemory) != .SUCCESS{
		return .MemoryAllocationError
	}

    vk.BindBufferMemory(ctx.device, buffer^, bufferMemory^, 0)
    return nil
}

copyBuffer :: proc (ctx: ^Context, src, dst: vk.Buffer, size: vk.DeviceSize) {
	commandBuffer := beginSingleTimeCommands(ctx)

	copyRegion := vk.BufferCopy {
		srcOffset = 0,
		dstOffset = 0,
		size = size,
	}
	vk.CmdCopyBuffer(commandBuffer, src, dst, 1, &copyRegion)

	endSingleTimeCommands(ctx, &commandBuffer)
}

createSyncObjects :: proc (ctx: ^Context) -> Error {
    semaphoreInfo := vk.SemaphoreCreateInfo{sType = .SEMAPHORE_CREATE_INFO,}
    fenceInfo := vk.FenceCreateInfo{
		sType = .FENCE_CREATE_INFO,
		flags = {.SIGNALED},
	}

    for i in 0..<MAX_FRAMES_IN_FLIGHT {
		if vk.CreateSemaphore(ctx.device, &semaphoreInfo, nil, &ctx.imageAvailableSemaphores[i]) != .SUCCESS||
			vk.CreateSemaphore(ctx.device, &semaphoreInfo, nil, &ctx.renderFinishedSemaphores[i]) != .SUCCESS||
			vk.CreateFence(ctx.device, &fenceInfo, nil, &ctx.inFlightFences[i]) != .SUCCESS{
				return .SyncObjectCreationError
			}
	}
    return nil
}

findSupportedFormat :: proc (ctx: ^Context, candidates: []vk.Format, tiling: vk.ImageTiling, features: vk.FormatFeatureFlags) -> (vk.Format, Error) {
	for format in candidates {
		props : vk.FormatProperties
		vk.GetPhysicalDeviceFormatProperties(ctx.physicalDevice, format, &props)
		if tiling == .LINEAR && props.linearTilingFeatures >= features do return format, nil
		else if tiling == .OPTIMAL && props.optimalTilingFeatures >= features do return format, nil
	}
	return {}, .NoFormatFound
}

findDepthFormat :: proc(ctx: ^Context) -> (vk.Format, Error) {
	return findSupportedFormat(
		ctx,
		{.D32_SFLOAT, .D32_SFLOAT_S8_UINT, .D24_UNORM_S8_UINT},
			.OPTIMAL,
		{.DEPTH_STENCIL_ATTACHMENT},
	)
}

hasStencilComponent :: proc(format: vk.Format) -> bool {
	return format == .D32_SFLOAT_S8_UINT || format == .D24_UNORM_S8_UINT
}

findMemoryType :: proc (ctx: ^Context, typeFilter: u32, props: vk.MemoryPropertyFlags) -> (res: u32, err: Error) {
	memProps : vk.PhysicalDeviceMemoryProperties
	vk.GetPhysicalDeviceMemoryProperties(ctx.physicalDevice, &memProps)

	for i in 0..<memProps.memoryTypeCount {
		if (typeFilter & (1 << i)) != 0 && props <= memProps.memoryTypes[i].propertyFlags do return i, nil
	}
	return 0, .NoSuitableMemoryError
}

@(cold)
recreateSwapChain :: proc (ctx: ^Context) -> Error {
	vk.DeviceWaitIdle(ctx.device)

	cleanupSwapChain(ctx)
	createSwapChain(ctx) or_return
	createImageViews(ctx) or_return
	createDepthResources(ctx) or_return
	createFramebuffers(ctx) or_return
	return nil
}

cleanupSwapChain :: proc (ctx: ^Context) {
	for frameBuffer in ctx.swapChainFramebuffers {
		vk.DestroyFramebuffer(ctx.device, frameBuffer, nil)
	}
	for imageView in ctx.swapChainImageViews {
		vk.DestroyImageView(ctx.device, imageView, nil)
	}

	vk.DestroyImageView(ctx.device, ctx.depthImageView, nil)
	vk.DestroyImage(ctx.device, ctx.depthImage, nil)
	vk.FreeMemory(ctx.device, ctx.depthImageMemory, nil)

	vk.DestroySwapchainKHR(ctx.device, ctx.swapChain, nil)
}

deviceIsSuitable :: proc (device: vk.PhysicalDevice, ctx: ^Context) -> bool {
	deviceProperties : vk.PhysicalDeviceProperties
	deviceFeatures : vk.PhysicalDeviceFeatures

    vk.GetPhysicalDeviceProperties(device, &deviceProperties)
    vk.GetPhysicalDeviceFeatures(device, &deviceFeatures)

    //TODO Suitability Algorithm
    qf := findQueueFamilies(device, ctx.surface)

    deviceSupportsExtensions := checkExtensionSupport(device)
    swapChainAdequate := false
    if deviceSupportsExtensions {
		swapChainSupport := querySwapChainSupport(device, ctx.surface)
		swapChainAdequate = len(swapChainSupport.formats) > 0 &&
			len(swapChainSupport.presentModes) > 0
	}
    suitable := isComplete(qf) && deviceSupportsExtensions && swapChainAdequate && deviceFeatures.samplerAnisotropy
    if suitable {
		fmt.eprintf("%s selected!\n", cstring(raw_data(&deviceProperties.deviceName)))
	}

    return suitable
}

checkExtensionSupport :: proc (device: vk.PhysicalDevice) -> bool {
    availableExtensionCount : u32
    vk.EnumerateDeviceExtensionProperties(device, nil, &availableExtensionCount, nil)

    availableExtensions := make([]vk.ExtensionProperties, availableExtensionCount)
    vk.EnumerateDeviceExtensionProperties(device, nil, &availableExtensionCount, &availableExtensions[0])

    for extension in deviceExtensions {
		found := false
		for i in 0..< len(availableExtensions) {
			if (strings.compare(string(cstring(raw_data(&availableExtensions[i].extensionName))),
								string(extension)) == 0){
				found = true
				break
			}
		}
		if !found do return false
	}
    return true
}

findQueueFamilies :: proc (device: vk.PhysicalDevice, surface: vk.SurfaceKHR) -> QueueFamilyIndices {
    indices : QueueFamilyIndices

    vk.GetPhysicalDeviceQueueFamilyProperties(device, &indices.familyCount, nil)
    qFamilies := make([^]vk.QueueFamilyProperties, indices.familyCount)
    vk.GetPhysicalDeviceQueueFamilyProperties(device, &indices.familyCount, qFamilies)

    for i in 0..<indices.familyCount {
		if (.GRAPHICS in qFamilies[i].queueFlags){
			indices.Graphics = i
		}
		presentSupport: b32 = false
		vk.GetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport)
		if presentSupport do indices.Present = i
		if isComplete(indices) do break
	}

    return indices
}

querySwapChainSupport :: proc (device: vk.PhysicalDevice, surface: vk.SurfaceKHR) -> (res: SwapChainSupportDetails) {
    vk.GetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &res.capabilities)

    formatCount : u32
    vk.GetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nil)

    if formatCount != 0 {
		res.formats = make([]vk.SurfaceFormatKHR, formatCount)
		vk.GetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, &res.formats[0])
	}

    presentModeCount : u32
    vk.GetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nil)

    if presentModeCount != 0 {
		res.presentModes = make([]vk.PresentModeKHR, presentModeCount)
		vk.GetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, &res.presentModes[0])
	}
    return res
}

chooseSwapSurfaceFormat :: proc (availableFormats: []vk.SurfaceFormatKHR) -> vk.SurfaceFormatKHR {
    for format in availableFormats {
		if format.format == .B8G8R8A8_SRGB &&
			format.colorSpace == .SRGB_NONLINEAR {
				return format
			}
	}
    return availableFormats[0]
}

chooseSwapPresentMode :: proc (availablePresentModes: []vk.PresentModeKHR) -> vk.PresentModeKHR {
	for mode in availablePresentModes {
		if mode == .MAILBOX do return mode
	}
	return .FIFO
}

createShaderModule :: proc (ctx: ^Context, code: []u32) -> (sm: vk.ShaderModule, err: Error) {
	createinfo := vk.ShaderModuleCreateInfo{
		sType = .SHADER_MODULE_CREATE_INFO,
		codeSize = len(code),
		pCode = &code[0],
	}

	if vk.CreateShaderModule(ctx.device, &createinfo, nil, &sm) != .SUCCESS {
		return {}, .ShaderModuleCreationError
	}
	return sm, nil
}
