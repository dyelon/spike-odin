package window

import "vendor:x11/xlib"
import "core:c"
import "core:fmt"
import "../x11"

Context :: struct {
	win: xlib.Window,
	dpy: ^xlib.Display,
	gc: xlib.GC,
	width: u32,
	height: u32,
	depth: i32,
	input: Input,
	visual: ^xlib.Visual
}

Input :: struct {
	keyW: bool,
	keyA: bool,
	keyS: bool,
	keyD: bool,
	keySpace: bool,
	keyCtrl: bool,
	keyShift: bool,
}

Error :: enum {
	None,
	GetVisualError,
	ThreadInitError,
}

DELETE_ATOM : xlib.Atom

init :: proc (width: u32 = 1920, height: u32 = 1080, depth: i32 = 32, name: cstring) -> (ctx: Context, err: Error) {
    using xlib

    if XInitThreads() == .Success {
		//NOTE This is actually failure
		return {}, .ThreadInitError
	}

	ctx.dpy = XOpenDisplay(nil)
	ctx.width = width
	ctx.height = height
	ctx.depth = depth

    parent: Window = XDefaultRootWindow(ctx.dpy)
    //vis := get_visual(result.dpy, c.int(depth))
	vis := x11.XRenderGetVisual(ctx.dpy, depth)
	if vis == nil do return {}, .GetVisualError
	ctx.visual = vis

	attrs := XSetWindowAttributes {
		backing_store = .NotUseful,  //Default value
		override_redirect = false,		   //TODO Check this value
		colormap = XCreateColormap(ctx.dpy, parent, vis, .AllocNone),
		border_pixel = 0, //Black border or no border?
		background_pixmap = None,
		bit_gravity = .NorthWestGravity, /* This specifies which corner will be retained during a resize */
		win_gravity = .NorthWestGravity, /* Same as above but for the window when its parent is resized */
		save_under = false, /* Not a useful flag for our purposes */
		do_not_propagate_mask = {}, //NoEventMask
		event_mask =	{
				.KeyPress, /* List of events we want to recieve */
				.KeyRelease,
				.ButtonPress,
				.ButtonRelease,
				.EnterWindow,
				.LeaveWindow,
				.PointerMotion,
				.Exposure,
				.VisibilityChange,
				.StructureNotify,
				.FocusChange,
				.PropertyChange,
				.ColormapChange
		},
	}

    ctx.win = XCreateWindow(ctx.dpy, parent,
							1400, 620, ctx.width, ctx.height, 0,
							depth,
							.InputOutput,
							vis,
							{
									.CWBackingStore,
									.CWOverrideRedirect,
									.CWColormap,
									.CWBorderPixel,
									.CWBackPixmap,
									.CWSaveUnder,
									.CWDontPropagate,
									.CWEventMask,
									.CWBitGravity,
									.CWWinGravity
							},
							&attrs)



	classHint := xlib.XClassHint {
		res_name = name,
		res_class = "__dev__"
	}
    XMapWindow(ctx.dpy, ctx.win)
    ctx.gc = XCreateGC(ctx.dpy, ctx.win, {}, nil)
    XSetWindowBackground(ctx.dpy, ctx.win, 0xB01f1f1f)
	XSetClassHint(ctx.dpy, ctx.win, &classHint)

    ev: XEvent = {}
    for XPending(ctx.dpy) > 0{
		XNextEvent(ctx.dpy, &ev)
		if ev.type == EventType.MapNotify {
			break;
		}
	}

	DELETE_ATOM = XInternAtom(ctx.dpy, "WM_DELETE_WINDOW", false)
	XSetWMProtocols(ctx.dpy, ctx.win, &DELETE_ATOM, 1)


    return ctx, nil
}

//TODO Gather the events that need to be platform agnostic and send them
processEvents :: proc (ctx: ^Context) -> bool {
	using xlib
	shouldClose := false
	ev : XEvent = {}
	for XPending(ctx.dpy) > 0 {
		XNextEvent(ctx.dpy, &ev)
		#partial switch ev.type {
			case .KeyPress:
			switch ev.xkey.keycode{
			case u32(XKeysymToKeycode(ctx.dpy, .XK_Q)):
				shouldClose = true
			case u32(XKeysymToKeycode(ctx.dpy, .XK_W)):
				ctx.input.keyW = true
			case u32(XKeysymToKeycode(ctx.dpy, .XK_A)):
				ctx.input.keyA = true
			case u32(XKeysymToKeycode(ctx.dpy, .XK_S)):
				ctx.input.keyS = true
			case u32(XKeysymToKeycode(ctx.dpy, .XK_D)):
				ctx.input.keyD = true
			case u32(XKeysymToKeycode(ctx.dpy, .XK_space)):
				ctx.input.keySpace = true
			case u32(XKeysymToKeycode(ctx.dpy, .XK_Control_L)):
				ctx.input.keyCtrl = true
			case u32(XKeysymToKeycode(ctx.dpy, .XK_Shift_L)):
				ctx.input.keyShift = true
			}
			case .KeyRelease:
			switch ev.xkey.keycode{
			case u32(XKeysymToKeycode(ctx.dpy, .XK_W)):
				ctx.input.keyW = false
			case u32(XKeysymToKeycode(ctx.dpy, .XK_A)):
				ctx.input.keyA = false
			case u32(XKeysymToKeycode(ctx.dpy, .XK_S)):
				ctx.input.keyS = false
			case u32(XKeysymToKeycode(ctx.dpy, .XK_D)):
				ctx.input.keyD = false
			case u32(XKeysymToKeycode(ctx.dpy, .XK_space)):
				ctx.input.keySpace = false
			case u32(XKeysymToKeycode(ctx.dpy, .XK_Control_L)):
				ctx.input.keyCtrl = false
			case u32(XKeysymToKeycode(ctx.dpy, .XK_Shift_L)):
				ctx.input.keyShift = false
			}
			case .ClientMessage:
			if transmute(xlib.Atom)ev.xclient.data.l[0] == DELETE_ATOM do shouldClose = true
			case .ConfigureNotify:
			ctx.width = u32(ev.xconfigure.width)
			ctx.height = u32(ev.xconfigure.height)
		}
	}

	return shouldClose
}

hideCursor :: proc (ctx: ^Context){
	using xlib
	x11.XFixesHideCursor(ctx.dpy, ctx.win)
}

warpCursorToCenter :: proc(ctx: ^Context){
	xlib.XWarpPointer(ctx.dpy, {}, ctx.win, 0, 0, ctx.width, ctx.height, i32(ctx.width)/2, i32(ctx.height)/2)
}


closeWindow :: proc (ctx: ^Context) {
	xlib.XDestroyWindow(ctx.dpy, ctx.win);
	when ODIN_DEBUG{
		fmt.eprintf("Window closed\n")
	}
}
