package main

import "window"
import "vulkan"
import "core:fmt"
import "core:os"
import "core:math"
import la "core:math/linalg"

main :: proc () {
    using os
	wctx, werr := window.init(name = "hello")
	if werr != nil {
		fmt.eprintf("X11 Failed to initialise: %s\n", werr)
		return
	}
	window.hideCursor(&wctx)

	vctx, vkerr := vulkan.init("hello", &wctx)
	if vkerr != nil {
		fmt.eprintf("Vulkan failed to initialise: %s\n", vkerr)
		return
	}
	defer vulkan.deinit(&vctx)

    fd, file_ok := open("/dev/input/event22", O_NONBLOCK | O_RDONLY)
    ev : input_event
    fmt.eprintf("Size of ev = %d\n", size_of(ev))
    x, y : i32
	if file_ok != ERROR_NONE do return

	shouldClose := false
	camera := vulkan.initCamera()

	for !shouldClose {
		using la
		totalread, read_ok := read_ptr(fd, &ev, size_of(ev))
		if totalread > 0{
			if ev.type == .EV_REL {
				if ev.code == 0 do camera.yaw -= f32(i32(ev.value))/1000
				else if ev.code == 1 do camera.pitch -= f32(i32(ev.value))/1000
			}
		}
		camera.pitch = clamp(camera.pitch, -math.PI/2+0.1, math.PI/2-0.1)
		shouldClose = window.processEvents(&wctx);
		speed : f32 = 0.0001
		if wctx.input.keyShift do speed = 0.01
		if wctx.input.keyW do camera.pos += speed * camera.front
		if wctx.input.keyA do camera.pos -= speed * normalize(cross(camera.front, camera.up))
		if wctx.input.keyS do camera.pos -= speed * camera.front
		if wctx.input.keyD do camera.pos += speed * normalize(cross(camera.front, camera.up))
		if wctx.input.keySpace do camera.pos -= speed * camera.up
		if wctx.input.keyCtrl do camera.pos += speed * camera.up
		window.warpCursorToCenter(&wctx)
		vulkan.drawFrame(&vctx, &camera)
		ev = {}
	}
	window.closeWindow(&wctx)
}

timeval :: struct {
	tv_sec : i64,
	tv_usec : i64,
}

EventType :: enum u16 {
    EV_SYN = 0,
    EV_KEY = 0x1,
    EV_REL = 0x2,
    EV_ABS = 0x3,
    EV_MSC = 0x4,
    EV_SW = 0x5,
    EV_LED = 0x11,
    EV_SND = 0x12,
    EV_REP = 0x14,
	EV_FF = 0x15,
	EV_PWR = 0x16,
	EV_FF_STATUS = 0x17,
	EV_MAX = 0x1f,
	EV_CNT = EV_MAX+1,
}

input_event :: struct {
	time: timeval,
	type: EventType ,
	code: u16,
	value: u32,
}
