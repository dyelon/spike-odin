#! /bin/bash

APPLICATION_NAME='spike'

#DEFINES GO HERE
DEFINE="GLFW=false"

RUN=0
OPT="-o:minimal"
LIBS='-lX11 -lasound'
DEBUG=""
MDEBUG=""

while [ -n "$1" ]; do

	case "$1" in

		-g) DEBUG="-debug" ;;
		
		-o) OPT="-o:speed" ;;

		-r) RUN=1 ;;

		*) echo "Options are -g -o -r";;

	esac

	shift

done

mkdir -p bin/
if [ "${DEBUG}" != "" ]; then
	MDEBUG=" debug"
fi
make && \
odin build src/ -out:bin/${APPLICATION_NAME} -extra-linker-flags:"${LIBS}" ${DEBUG} ${OPT} -define:${DEFINE} -show-timings

EXITCODE=$?

if [ ${EXITCODE} -eq 0 ] && [ $RUN -eq 1 ]
then
	bin/${APPLICATION_NAME}
else
	exit ${EXITCODE}
fi
